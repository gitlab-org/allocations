require File.expand_path('../lib/allocations/version', __FILE__)

Gem::Specification.new do |gem|
  gem.name        = 'allocations'
  gem.version     = Allocations::VERSION
  gem.authors     = ['Yorick Peterse']
  gem.email       = 'yorickpeterse@gmail.com'
  gem.summary     = 'Tracking of retained objects in CRuby'
  gem.homepage    = 'https://gitlab.com/gitlab-org/allocations'
  gem.description = gem.summary
  gem.license     = 'MIT'
  gem.extensions  = ['ext/liballocations/extconf.rb']

  gem.files = Dir.glob([
    'lib/**/*.rb',
    'ext/**/*',
    'README.md',
    'LICENSE',
    '*.gemspec'
  ]).select { |file| File.file?(file) }

  gem.has_rdoc              = 'yard'
  gem.required_ruby_version = '>= 2.1.0'

  gem.add_development_dependency 'rake'
  gem.add_development_dependency 'rake-compiler'
  gem.add_development_dependency 'benchmark-ips', ['~> 2.0']
  gem.add_development_dependency 'rspec', ['~> 3.0']
end
