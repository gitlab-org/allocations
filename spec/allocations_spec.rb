require 'spec_helper'

describe Allocations do
  after do
    described_class.stop
  end

  describe '.start' do
    it 'starts the counting of allocations' do
      described_class.start

      expect(described_class).to be_enabled
    end

    it 'allows consecutive calls' do
      described_class.start
      described_class.start

      expect(described_class).to be_enabled
    end
  end

  describe '.stop' do
    it 'stops the counting of allocations' do
      described_class.start
      described_class.stop

      expect(described_class).to_not be_enabled
    end
  end

  describe '.enabled?' do
    it 'returns true when counting is enabled' do
      described_class.start

      expect(described_class.enabled?).to eq(true)
    end

    it 'returns false when counting is disabled' do
      expect(described_class.enabled?).to eq(false)
    end
  end

  describe '.to_hash' do
    before do
      described_class.start
    end

    it 'returns a Hash containing object counts' do
      hash = described_class.to_hash

      expect(hash).to be_an_instance_of(Hash)
      expect(hash.keys).not_to be_empty
      expect(hash.values[0] > 0).to eq(true)
    end

    it 'does not include singleton classes in the returned Hash' do
      klass = Class.new
      hash = described_class.to_hash

      expect(hash.keys).not_to include(klass.singleton_class)
    end
  end
end
