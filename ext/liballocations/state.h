#ifndef ALLOCATIONS_STATE_H
#define ALLOCATIONS_STATE_H

#include "liballocations.h"

typedef struct {
    st_table *object_counts;
} AllocationState;

extern void allocation_state_reset_counts(AllocationState*);
extern void allocation_state_allocate_counts(AllocationState*);
extern AllocationState *allocation_state_get_struct(VALUE object);
extern st_table *allocation_state_copy_table(AllocationState*);

extern void Init_allocations_state();

#endif
