require_relative 'benchmark_helper'

Allocations.start

strings = 10_000.times.map { 'a' * 32 }

Benchmark.ips do |bench|
  bench.report 'ObjectSpace' do
    ObjectSpace.each_object(String).count
  end

  bench.report 'Allocations' do
    Allocations.to_hash[String]
  end

  bench.compare!
end

Allocations.stop
