require_relative 'benchmark_helper'

Benchmark.ips do |bench|
  bench.report 'disabled' do
    'a' * 32
  end

  bench.report 'enabled' do |times|
    Allocations.start

    i = 0

    while i < times
      'a' * 32
      i += 1
    end

    Allocations.stop
  end

  bench.compare!
end
